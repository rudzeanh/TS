import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class MoodleSelenium {

    WebDriver driver;
    WebDriverWait wait;

    @BeforeEach
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver, Duration.ofSeconds(20));
    }

    @Test
    public void doTheQuiz() {
        String login = "";
        String password = "";

        driver.get("https://moodle.fel.cvut.cz/mod/quiz/view.php?id=233484");

        // click SSO login
        driver.findElement(By.id("sso-form")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("cvut-main-content")));

        // after redirect enter login and password
        driver.findElement(By.id("username")).sendKeys(login);
        driver.findElement(By.id("password")).sendKeys(password);

        // click SSO login
        driver.findElement(By.cssSelector("button[type=submit]")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("region-main")));

        // now the quiz page is opened
        // click to start the quiz
        driver.findElement(By.cssSelector("button[type=submit]")).click();
        // accept the rules
        driver.findElement(By.id("id_submitbutton")).click();

        // fill the fields
        driver.findElement(By.cssSelector(".essay .answer textarea"))
                .sendKeys("Anhelina Rudzenka - paralelka 102 - cvičení 11");

        driver.findElement(By.cssSelector(".numerical .answer input")).sendKeys("86400");

        List<WebElement> selects = driver.findElements(By.cssSelector("select"));

        Select planetsQuestion = new Select(selects.get(0));
        planetsQuestion.selectByVisibleText("Oberon");

        Select geographyQuestion = new Select(selects.get(1));
        geographyQuestion.selectByVisibleText("Rumunsko");

        // finish the test - optional
        // driver.findElement(By.cssSelector(".submitbtns input[type=submit]")).click();
    }
}
