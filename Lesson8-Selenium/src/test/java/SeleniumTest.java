import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class SeleniumTest {

    WebDriver driver;

    @BeforeEach
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\CVUT\\TS\\chromedriver.exe");
        driver = new ChromeDriver();
    }

    @Test
    public void flightFormTest_Lesson1() {
        String name = "Angelina";
        String surname = "Rudenko";
        String email = "myEmail@gmail.com";
        String birthDate = "06122002";

        driver.get("https://flight-order.herokuapp.com");

        // use it if you need more time
//        WebElement firstResult = new WebDriverWait(driver,
//                Duration.ofSeconds(10)).until(
//                ExpectedConditions.elementToBeClickable(By.xpath("//a/h3")));

        driver.findElement(By.id("flight_form_firstName")).sendKeys(name);
        driver.findElement(By.id("flight_form_lastName")).sendKeys(surname);
        driver.findElement(By.id("flight_form_email")).sendKeys(email);
        driver.findElement(By.id("flight_form_birthDate")).sendKeys(birthDate);
        driver.findElement(By.id("flight_form_discount_0")).click();

        Select selectDestination = new Select(driver.findElement(By.id("flight_form_destination")));
        selectDestination.selectByVisibleText("London");

        // driver.findElement(By.cssSelector("body > div > div > form > div > button")).click();
        driver.findElement(By.id("flight_form_submit")).click();

        // If you have a small window
//        Actions a = new Actions(driver);
//        a.moveToElement(btn);
//        a.perform();
//        btn.click();

        // Checks the title of web page

        WebElement fullName = driver.findElement(By.cssSelector("tr:nth-child(1) td"));

        //Assertions.assertEquals("Done!", driver.getTitle());
        Assertions.assertEquals(name + " " + surname, fullName.getAttribute("textContent"));

        driver.quit(); // to close the browser window
    }

    @Test
    public void flightFormTest_Lesson2() {
        String name = "Angelina";
        String surname = "Rudenko";
        String email = "myEmail@gmail.com";
        String birthDate = "06122002";

        FlightFormPage flightFormPage = new FlightFormPage(driver);

        flightFormPage.setName(name);
        flightFormPage.setSurname(surname);
        flightFormPage.setEmail(email);
        flightFormPage.setBirthDate(birthDate);

        flightFormPage.sendForm();

        WebElement fullName = driver.findElement(By.cssSelector("tr:nth-child(1) td"));
        Assertions.assertEquals(name + " " + surname, fullName.getAttribute("textContent"));

        driver.quit(); // to close the browser window
    }
}
