import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FlightFormPage {

    WebDriver driver;

    @FindBy(id = "flight_form_firstName")
    WebElement name;
    @FindBy(id = "flight_form_lastName")
    WebElement surname;
    @FindBy(id = "flight_form_email")
    WebElement email;
    @FindBy(id = "flight_form_birthDate")
    WebElement birthDate;
    @FindBy(id = "flight_form_submit")
    WebElement sendButton;

    public FlightFormPage(WebDriver driver) {
        this.driver = driver;
        driver.get("https://flight-order.herokuapp.com");
        PageFactory.initElements(driver, this);
    }

    public void setName(String name) {
        this.name.sendKeys(name);
    }

    public void setSurname(String surname) {
        this.surname.sendKeys(surname);
    }

    public void setEmail(String email) {
        this.email.sendKeys(email);
    }

    public void setBirthDate(String birthDate) {
        this.birthDate.sendKeys(birthDate);
    }

    public void sendForm() {
        sendButton.click();
    }
}
