package Lesson4;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalculatorTest {

    // !!! Testy s databázi nejsou jendotkové

    /**
     * Parametrizace vstupních dat
     */
    @ParameterizedTest(name = "{0} plus {1} should be equal to {2}")
    @CsvSource({"1, 2, 3", "2, 3, 5"})
    public void plus_sumTwoNumbersFromAttribute_returnsExpectedResult(int a, int b, int c) {
        // Arrange
        int expectedResult = c;

        // Act
        int result = Calculator.plus(a, b);

        // Assert
        assertEquals(expectedResult, result);
    }

    @ParameterizedTest(name = "{0} plus {1} should be equal to {2}")
    @CsvFileSource(resources = "/data.csv", numLinesToSkip = 1)
    public void plus_sumTwoNumbersFromFile_returnsExpectedResult(int a, int b, int c) {
        // Arrange
        int expectedResult = c;

        // Act
        int result = Calculator.plus(a, b);

        // Assert
        assertEquals(expectedResult, result);
    }
}
