package cz.cvut.fel.ts1.refactoring;

import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

public class MockMailTest {

    @Test
    public void sandMail_validEmail_saveMailToDB() {
        DBManager mockedDbManager = mock(DBManager.class);
        MailHelper mailHelper = new MailHelper(mockedDbManager);
        int mailId = 1;

        Mail mailToReturn = mock(Mail.class);

        when(mailToReturn.getTo()).thenReturn("Exists");

        when(mockedDbManager.findMail(anyInt())).thenReturn(mailToReturn);

        mailHelper.sendMail(mailId);

        verify(mockedDbManager, times(1)).findMail(mailId);
        // verify(mockedDbManager).findMail(anyInt());
    }
}
