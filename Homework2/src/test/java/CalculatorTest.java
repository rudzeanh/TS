import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    @Test
    public void Plus_ReturningSumOfPositiveTerms_1097() {
        // arrange
        double fistTerm = 1095;
        double secondTerm = 2;
        double expectedResult = 1097;

        // act
        double actualResult = Calculator.Plus(fistTerm, secondTerm);

        // assert
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void Minus_ReturningDifferenceOfNumbers_minus1000() {
        // arrange
        double fistTerm = 1000;
        double secondTerm = 3000;
        double expectedResult = -2000;

        // act
        double actualResult = Calculator.Minus(fistTerm, secondTerm);

        // assert
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void Division_ReturningDivisionOfNumbers_minus2() throws Exception {
        // arrange
        double fistTerm = -10;
        double secondTerm = 5;
        double expectedResult = -2;

        // act
        double actualResult = Calculator.Division(fistTerm, secondTerm);

        // assert
        Assertions.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void Division_ThrowingExceptionAboutDivisionByZero_Exception() {
        // arrange
        double fistTerm = 1;
        double secondTerm = 0;

        // assert
        Assertions.assertThrows(Exception.class, () -> {
            double result = Calculator.Division(fistTerm, secondTerm);
        });
    }

    @Test
    public void Multiplication_ReturningMultiplicationOfNumbers_30() throws Exception {
        // arrange
        double fistTerm = 15;
        double secondTerm = 2;
        double expectedResult = 30;

        // act
        double actualResult = Calculator.Multiplication(fistTerm, secondTerm);

        // assert
        Assertions.assertEquals(expectedResult, actualResult);
    }
}
