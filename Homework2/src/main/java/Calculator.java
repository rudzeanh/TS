public class Calculator {

    // +
    public static double Plus(double x, double y) {
        return x + y;
    }

    // -
    public static double Minus(double x, double y) {
        return x - y;
    }

    // /
    public static double Division(double x, double y) throws Exception {
        if (y == 0) {
            throw new Exception("Can't divide by zero. Uncertainty.");
        }
        return x/y;
    }

    // *
    public static double Multiplication(double x, double y) {
        return x * y;
    }
}
