package storage;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import shop.StandardItem;

public class ItemStockTest {

    StandardItem standardItem;
    ItemStock itemStock;

    @BeforeEach
    public void prepareItemStock() {
        standardItem = new StandardItem(1, "Apple", 3.6f, "Fruit", 5);
        itemStock = new ItemStock(standardItem);
    }

    @Test
    public void constructor_CreatesObjectWithParameters_ObjectWithCorrectParameters() {
        Assertions.assertEquals(standardItem, itemStock.getItem());
        Assertions.assertEquals(0, itemStock.getCount());
    }

    @Test
    public void constructor_CreatesObjectWithNullItem_ObjectWithNullRefItem() {
        ItemStock itemStock = new ItemStock(null);

        Assertions.assertNull(itemStock.getItem());
        Assertions.assertEquals(0, itemStock.getCount());
    }

    @ParameterizedTest(name = "Item count should increase by {0}")
    @CsvFileSource(resources = "/ItemStock_increaseItemCountOnce.csv", numLinesToSkip = 0)
    public void increaseItemCount_IncreaseItemStockItemCountOnce_CorrectCount(int increaseByNumber) {
        itemStock.IncreaseItemCount(increaseByNumber);

        Assertions.assertEquals(increaseByNumber, itemStock.getCount());
    }

    @ParameterizedTest(name = "Item count should increase by {0}, then by {1}. Expected result {2}")
    @CsvFileSource(resources = "/ItemStock_increaseItemCountTwoTimes.csv", numLinesToSkip = 0)
    public void increaseItemCount_IncreaseItemStockItemCountTwoTimes_CorrectCount(
            int increaseByFirstNumber, int increaseBySecondNumber, int expectedResult) {

        itemStock.IncreaseItemCount(increaseByFirstNumber);
        itemStock.IncreaseItemCount(increaseBySecondNumber);

        Assertions.assertEquals(expectedResult, itemStock.getCount());
    }

    @ParameterizedTest(name = "Item count should increase by {0}, then decrease by {1}. Expected result {2}")
    @CsvFileSource(resources = "/ItemStock_decreaseItemCountOnce.csv", numLinesToSkip = 0)
    public void decreaseItemCount_DecreaseItemStockItemCountOnce_CorrectCount(
            int increaseByNumber, int decreaseBySecondNumber, int expectedResult) {

        itemStock.IncreaseItemCount(increaseByNumber);
        itemStock.decreaseItemCount(decreaseBySecondNumber);

        Assertions.assertEquals(expectedResult, itemStock.getCount());
    }
}
