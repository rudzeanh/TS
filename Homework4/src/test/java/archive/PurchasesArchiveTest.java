package archive;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import shop.Item;
import shop.Order;
import shop.StandardItem;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class PurchasesArchiveTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private StandardItem standardItemMock;
    private ItemPurchaseArchiveEntry itemPurchaseArchiveEntryMock;
    private HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive;
    private HashMap<Integer, ItemPurchaseArchiveEntry> itemArchiveMock;
    private Order orderMock;
    private ArrayList<Order> orderArchiveMock;
    private int randomInt;

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outContent));

        Random random = new Random();
        randomInt = random.nextInt();

        standardItemMock = Mockito.mock(StandardItem.class);
        itemPurchaseArchiveEntryMock = Mockito.mock(ItemPurchaseArchiveEntry.class);
        orderMock  = Mockito.mock(Order.class);
        // Mocked orderArchive
        orderArchiveMock = Mockito.mock(ArrayList.class);
        // Or it could be like this:
        // orderArchiveMock = new ArrayList<>() {
        //      { add(orderMock); }
        // };

        itemArchive = new HashMap<>() {
            { put(randomInt, itemPurchaseArchiveEntryMock); }
        };
        itemArchiveMock = Mockito.mock(HashMap.class);
    }

    /**
     * This tests for both:
     * 1. Constructor with no parameters
     * 2. printItemPurchaseStatistics method, when purchase archive is empty
     */
    @Test
    public void printItemPurchaseStatistics_PrintItemPurchaseArchiveWithEmptyItemArchive_CorrectOutput() {
        PurchasesArchive purchasesArchive = new PurchasesArchive();

        purchasesArchive.printItemPurchaseStatistics();
        Assertions.assertEquals("ITEM PURCHASE STATISTICS:" + System.lineSeparator(), outContent.toString());
    }

    @Test
    public void printItemPurchaseStatistics_PrintItemPurchaseArchive_CorrectOutput() {
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemArchive, null);

        Mockito.when(itemPurchaseArchiveEntryMock.toString()).thenReturn("Success");
        purchasesArchive.printItemPurchaseStatistics();

        Assertions.assertEquals("ITEM PURCHASE STATISTICS:" + System.lineSeparator() +
                "Success" + System.lineSeparator(), outContent.toString());
    }

    @Test
    public void getHowManyTimesHasBeenItemSold_ReturnNeverSoldItemCount_0() {
        StandardItem itemNeverSold = Mockito.mock(StandardItem.class);
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemArchiveMock, null);

        Mockito.when(itemNeverSold.getID()).thenReturn(878919149);

        Assertions.assertEquals(0, purchasesArchive.getHowManyTimesHasBeenItemSold(itemNeverSold));
    }

    @Test
    public void getHowManyTimesHasBeenItemSold_ReturnThreeTimesSoldItemCount_3() {
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemArchive, null);

        Mockito.when(standardItemMock.getID()).thenReturn(randomInt);
        Mockito.when(itemPurchaseArchiveEntryMock.getCountHowManyTimesHasBeenSold()).thenReturn(3);

        Assertions.assertEquals(3, purchasesArchive.getHowManyTimesHasBeenItemSold(standardItemMock));
    }

    @Test
    public void putOrderToPurchasesArchive_putNewOrder_orderAdded() {

        ArrayList<Item> items = new ArrayList<>() {
            { add(Mockito.mock(StandardItem.class)); }
        };

        orderMock.setItems(items);
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemArchiveMock, orderArchiveMock);

        Mockito.when(itemArchiveMock.containsKey(Mockito.anyInt())).thenReturn(false);

        Mockito.when(orderMock.getItems()).thenReturn(items);

        purchasesArchive.putOrderToPurchasesArchive(orderMock);

        Mockito.verify(itemArchiveMock, Mockito.times(1)).put(Mockito.anyInt(), Mockito.any(ItemPurchaseArchiveEntry.class));
    }

    @Test
    public void putOrderToPurchasesArchive_addOrderOfExistingItem_itemSoldCountIncreased() {
        StandardItem newItem = Mockito.mock(StandardItem.class);
        ArrayList<Item> items = new ArrayList<>() {
            { add(newItem); }
        };
        orderMock.setItems(items);

        PurchasesArchive purchasesArchive = new PurchasesArchive(itemArchive, orderArchiveMock);

        Mockito.when(orderMock.getItems()).thenReturn(items);
        Mockito.when(standardItemMock.getID()).thenReturn(randomInt);
        Mockito.when(newItem.getID()).thenReturn(randomInt);

        purchasesArchive.putOrderToPurchasesArchive(orderMock);

        Mockito.verify(itemPurchaseArchiveEntryMock, Mockito.times(1)).increaseCountHowManyTimesHasBeenSold(1);
    }

    @Test
    public void constructorWithTwoParameters_initializingTheInstance_objectWithEmptyFields() {
        PurchasesArchive purchasesArchive = new PurchasesArchive(itemArchiveMock, orderArchiveMock);

        // Check by references
        Assertions.assertSame(itemArchiveMock, purchasesArchive.getItemPurchaseArchive());
        Assertions.assertSame(orderArchiveMock, purchasesArchive.getOrderArchive());
    }
}
