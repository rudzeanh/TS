package shop;

import archive.PurchasesArchive;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import storage.NoItemInStorage;

public class EShopControllerProcessTest {

    /**
     * 1. Add 1 item of 1 type to shop storage
     * 2. Add item to cart
     * 3. Purchase shopping cart
    * */
    @Test
    public void butTheLastProduct() throws NoItemInStorage {
        EShopController.startEShop();

        // The storage is empty, has no items yet
        Assertions.assertEquals(0, EShopController.getItemsFromStorage().size());

        StandardItem product = new StandardItem(15236, "Apple", 4, "Fruit", 7);

        // Can't add to storage less than 1 item
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> EShopController.addItemToStorage(product, -5));

        EShopController.addItemToStorage(product, 1);

        // The storage now has 1 type of item
        Assertions.assertEquals(1, EShopController.getItemsFromStorage().size());

        // We have only one item of type Apple in our storage
        Assertions.assertEquals(1, EShopController.getItemsFromStorage().get(0).getCount());

        // Item in storage is the same as we added (check via reference)
        Assertions.assertSame(product, EShopController.getItemsFromStorage().get(0).getItem());

        ShoppingCart cart = EShopController.newCart();

        // Cart we got is added to E-Shop
        Assertions.assertTrue(EShopController.getCarts().contains(cart));

        // Only the one cart was added to the shop
        Assertions.assertEquals(1, EShopController.getCarts().size());

        // New cart is empty by default (has no items in it)
        Assertions.assertEquals(0, cart.getItemsCount());

        cart.addItem(product);

        // Now cart has 1 item in it
        Assertions.assertEquals(1, cart.getItemsCount());

        // And this item is a product, that we added
        Assertions.assertTrue(cart.getCartItems().contains(product));

        EShopController.purchaseShoppingCart(cart, "Angelina", "Chaloupeckeho");

        // Count of item of type left on storage = 0
        Assertions.assertEquals(0, EShopController.getItemsFromStorage().get(0).getCount());

        PurchasesArchive archive = EShopController.getArchive();
        Assertions.assertEquals(1, archive.getHowManyTimesHasBeenItemSold(product));
    }

    /**
     * 1. Add 5 types of items to shop storage. Every type has only 1 item.
     * 2. Add 3 types of items to cart. 1 item of each of 3 types.
     * 3. Delete item type from storage (the item of this type is in cart)
     * 4. Purchase shopping cart
     */
    @Test
    public void tryToBuyProductThatWasRemovedFromStorage() throws NoItemInStorage {
        EShopController.startEShop();

        // The storage is empty, has no items yet
        Assertions.assertEquals(0, EShopController.getItemsFromStorage().size());

        Item[] items = {
                new StandardItem(1, "Apple", 1, "Fruit", 1),
                new StandardItem(2, "Carrot", 5, "Vegetable", 2),
                new StandardItem(3, "Potato", 2, "Vegetable", 3),
                new StandardItem(4, "Cucumber", 3, "Vegetable", 4),
                new StandardItem(5, "Orange", 2, "Fruit", 5),
        };

        for (Item item : items) {
            // Can't add to storage less than 1 item
            Assertions.assertThrows(IllegalArgumentException.class,
                    () -> EShopController.addItemToStorage(item, -1));

            // Add 1 item of each type to storage
            EShopController.addItemToStorage(item, 1);

            // Every type is added only once
            Assertions.assertEquals(1, EShopController.getStorage().getItemCount(item));
        }

        // The storage now has 5 types of item
        Assertions.assertEquals(5, EShopController.getItemsFromStorage().size());

        ShoppingCart cart = EShopController.newCart();

        // Cart we got is added to E-Shop
        Assertions.assertTrue(EShopController.getCarts().contains(cart));

        // Only the one cart was added to the shop
        Assertions.assertEquals(1, EShopController.getCarts().size());

        // New cart is empty by default (has no items in it)
        Assertions.assertEquals(0, cart.getItemsCount());

        cart.addItem(items[0]);
        cart.addItem(items[2]);
        cart.addItem(items[4]);

        // Now cart has 3 items in it
        Assertions.assertEquals(3, cart.getItemsCount());

        // And those items are products, that we added
        Assertions.assertTrue(cart.getCartItems().contains(items[0]));
        Assertions.assertTrue(cart.getCartItems().contains(items[2]));
        Assertions.assertTrue(cart.getCartItems().contains(items[4]));

        // Delete item type from storage
        EShopController.getStorage().removeItems(items[0], 1);

        // Item was really deleted
        Assertions.assertEquals(0, EShopController.getStorage().getItemCount(items[0]));

        Assertions.assertThrows(NoItemInStorage.class,
                () -> EShopController.purchaseShoppingCart(cart, "Angelina", "Chaloupeckeho"));
    }
}
