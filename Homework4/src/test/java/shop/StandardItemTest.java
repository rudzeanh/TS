package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

public class StandardItemTest {

    @Test
    public void constructor_CreatesObjectWithParameters_ObjectWithCorrectParameters() {

        int expectedId = 1;
        String expectedName = "Apple";
        float expectedPrice = 3.6f;
        String expectedCategory = "Fruit";
        int expectedLoyaltyPoints = 5;

        StandardItem standardItem =
                new StandardItem(expectedId, expectedName, expectedPrice, expectedCategory, expectedLoyaltyPoints);

        int actualId = standardItem.getID();
        String actualName = standardItem.getName();
        float actualPrice = standardItem.getPrice();
        String actualCategory = standardItem.getCategory();
        int actualLoyaltyPoints = standardItem.getLoyaltyPoints();

        Assertions.assertEquals(expectedId, actualId);
        Assertions.assertEquals(expectedName, actualName);
        Assertions.assertEquals(expectedPrice, actualPrice);
        Assertions.assertEquals(expectedCategory, actualCategory);
        Assertions.assertEquals(expectedLoyaltyPoints, actualLoyaltyPoints);
    }

    @Test
    public void copy_CreatesTheObjectWithTheSameParameters_CopyWithCorrectParameters() {

        int expectedId = 1;
        String expectedName = "Apple";
        float expectedPrice = 3.6f;
        String expectedCategory = "Fruit";
        int expectedLoyaltyPoints = 5;

        StandardItem standardItem =
                new StandardItem(expectedId, expectedName, expectedPrice, expectedCategory, expectedLoyaltyPoints);

        StandardItem copyOfStandardItem = standardItem.copy();

        int actualId = copyOfStandardItem.getID();
        String actualName = copyOfStandardItem.getName();
        float actualPrice = copyOfStandardItem.getPrice();
        String actualCategory = copyOfStandardItem.getCategory();
        int actualLoyaltyPoints = copyOfStandardItem.getLoyaltyPoints();

        Assertions.assertEquals(expectedId, actualId);
        Assertions.assertEquals(expectedName, actualName);
        Assertions.assertEquals(expectedPrice, actualPrice);
        Assertions.assertEquals(expectedCategory, actualCategory);
        Assertions.assertEquals(expectedLoyaltyPoints, actualLoyaltyPoints);
    }

    @ParameterizedTest(name = "First StandardItem: id = {0}, name = {1}, price = {2}, category = {3}, loyalty points = {4}\n"+
        "Second First StandardItem: id = {5}, name = {6}, price = {7}, category = {8}, loyalty points = {9}\n"+
        "Are equal = {10}")
    @CsvFileSource(resources = "/StandardItem_equals.csv", numLinesToSkip = 0)
    public void equals_StandardItemEqualsStandardItemWithTheSameParameters_True(
            int firstId, String firstName, float firstPrice, String firstCategory, int firstLoyaltyPoints,
            int secondId, String secondName, float secondPrice, String secondCategory, int secondLoyaltyPoints,
            boolean expectedResultBool) {

        StandardItem firstStandardItem = new StandardItem(firstId, firstName, firstPrice, firstCategory, firstLoyaltyPoints);

        StandardItem secondStandardItem = new StandardItem(secondId, secondName, secondPrice, secondCategory, secondLoyaltyPoints);

        Assertions.assertEquals(expectedResultBool, firstStandardItem.equals(secondStandardItem));
    }

    @Test
    public void equals_StandardItemNotEqualsOtherThenStandardItemObject_False() {

        StandardItem standardItem = new StandardItem(1, "Apple", 3.6f, "Fruit", 5);

        ShoppingCart shoppingCart = new ShoppingCart();

        Assertions.assertFalse(standardItem.equals(shoppingCart));
    }
}
