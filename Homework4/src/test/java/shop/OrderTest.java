package shop;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class OrderTest {

    @Test
    public void constructorWithFourParameters_CreatesObjectWithParameters_ObjectWithCorrectParameters() {

        StandardItem firstStandardItem = new StandardItem(1, "Apple", 3.6f, "Fruit", 5);
        StandardItem secondStandardItem = new StandardItem(17, "Potato", 5.0f, null, 3);

        ArrayList<Item> expectedCartItems = new ArrayList<>();
        expectedCartItems.add(firstStandardItem);
        expectedCartItems.add(secondStandardItem);

        ShoppingCart cart = new ShoppingCart(expectedCartItems);
        String expectedCustomerName = "Angelina Pomidorovna";
        String expectedCustomerAddress = "Prague";
        int expectedState = 10;

        Order order = new Order(cart, expectedCustomerName, expectedCustomerAddress, expectedState);

        ArrayList<Item> actualItems = order.getItems();
        String actualCustomerName = order.customerName;
        String actualCustomerAddress = order.customerAddress;
        int actualState = order.getState();

        Assertions.assertEquals(expectedCartItems, actualItems);
        Assertions.assertEquals(expectedCustomerName, actualCustomerName);
        Assertions.assertEquals(expectedCustomerAddress, actualCustomerAddress);
        Assertions.assertEquals(expectedState, actualState);
    }

    @Test
    public void constructorWithThreeParameters_CreatesObjectWithParametersAndEmptyCart_ObjectWithCorrectParameters() {

        ArrayList<Item> expectedCartItems = new ArrayList<>();

        ShoppingCart cart = new ShoppingCart(expectedCartItems);
        String expectedCustomerName = "Angelina Pomidorovna";
        String expectedCustomerAddress = "Prague";
        int expectedState = 0;

        Order order = new Order(cart, expectedCustomerName, expectedCustomerAddress);

        ArrayList<Item> actualItems = order.getItems();
        String actualCustomerName = order.customerName;
        String actualCustomerAddress = order.customerAddress;
        int actualState = order.getState();

        Assertions.assertEquals(expectedCartItems, actualItems);
        Assertions.assertEquals(expectedCustomerName, actualCustomerName);
        Assertions.assertEquals(expectedCustomerAddress, actualCustomerAddress);
        Assertions.assertEquals(expectedState, actualState);
    }

    @Test
    public void constructorWithThreeParameters_CreatesObjectWithNullCart_ThrowsError() {
        Assertions.assertThrows(Exception.class, () ->
                new Order(null, null, null));
    }

    @Test
    public void constructorWithFourParameters_CreatesObjectWithNullCart_ThrowsError() {
        Assertions.assertThrows(Exception.class, () ->
                new Order(null, null, null, 5));
    }
}
