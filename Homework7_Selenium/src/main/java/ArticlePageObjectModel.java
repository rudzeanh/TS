import helpers.SpringerPageObjectModel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ArticlePageObjectModel extends SpringerPageObjectModel {

    @FindBy(className = "c-article-title")
    private WebElement articleTitleText;

    @FindBy(css = ".c-article-identifiers time")
    private WebElement articlePublishedDateText;

    @FindBy(css = ".c-bibliographic-information__list-item--doi .c-bibliographic-information__value")
    private WebElement articleDOIText;

    public ArticlePageObjectModel(WebDriver driver, String url, boolean reloadPage) {
        super(driver, url, reloadPage);
    }

    public ArticlePageObjectModel(WebDriver driver) {
        super(driver, "https://link.springer.com/article", true);
    }

    public String getArticleTitle() {
        return articleTitleText.getText();
    }

    public String getArticlePublishedDate() {
        return articlePublishedDateText.getText();
    }

    public String getArticleDOI() {
        return articleDOIText.getText();
    }
}
