import helpers.SpringerPageObjectModel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class HomepagePageObjectModel extends SpringerPageObjectModel {

    @FindBy(className = "register-link")
    private WebElement signupLoginButton;

    @FindBy(css = "button.open-search-options")
    private WebElement searchOptionsButton;

    @FindBy(id = "advanced-search-link")
    private WebElement advancedSearchButton;

    public HomepagePageObjectModel(WebDriver driver) {
        super(driver, "https://link.springer.com", true);
    }

    public void clickSignupLoginButton() {
        signupLoginButton.click();
        // set the timeout
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("login-box")));
    }

    public void clickSearchOptionsButton() {
        searchOptionsButton.click();
    }

    public void clickAdvancedSearchButtonButton() {
        advancedSearchButton.click();
        // set the timeout
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("advanced-search-form")));
    }
}
