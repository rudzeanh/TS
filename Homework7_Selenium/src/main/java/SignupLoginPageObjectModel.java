import helpers.SpringerPageObjectModel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class SignupLoginPageObjectModel extends SpringerPageObjectModel {

    /*
    * first name: Selenium
    * last name: Test
    * email: selenium-test@yopmail.com
    * password: qwerty12
    * */

    @FindBy(id = "login-box-email")
    private WebElement loginEmailTextField;

    @FindBy(id = "login-box-pw")
    private WebElement loginPasswordTextField;

    @FindBy(css = "button[title='Log in']")
    private WebElement logInButton;

    public SignupLoginPageObjectModel(WebDriver driver, String url, boolean reloadPage) {
        super(driver, url, reloadPage);
    }

    public SignupLoginPageObjectModel(WebDriver driver) {
        super(driver, "https://link.springer.com/signup-login", true);
    }

    public void writeLoginEmail(String email) {
        loginEmailTextField.sendKeys(email);
    }

    public void writeLoginPassword(String password) {
        loginPasswordTextField.sendKeys(password);
    }

    public void clickLogIn() {
        logInButton.click();
        // set the timeout
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#user > span")));
    }
}
