package helpers;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class SpringerPageObjectModel {

    protected WebDriver driver;

    @FindBy(css = "button.cc-banner__button-accept")
    private WebElement acceptCookiesButton;

    public SpringerPageObjectModel(WebDriver driver, String url, boolean reloadPage) {
        this.driver = driver;
        driver.manage().window().maximize();

        if (reloadPage)
            driver.get(url);

        PageFactory.initElements(driver, this);

        try {
            acceptCookiesButton.click();
        } catch (Exception ex) {
            System.out.println("No cookies banner found");
        }
    }

    public void goToThePreviousPage() {
        driver.navigate().back();
        // set the timeout
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(8));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("content")));
    }
}
