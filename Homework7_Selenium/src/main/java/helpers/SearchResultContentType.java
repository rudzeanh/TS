package helpers;

public class SearchResultContentType {

    public enum contentType {
        ARTICLE,
        CHAPTER,
        CONFERENCE_PAPER,
        REFERENCE_WORK_ENTRY,
        BOOK,
        PROTOCOL,
        CONFERENCE_PROCEEDINGS,
        BOOK_SERIES,
        JOURNAL,
        VIDEO_SEGMENTS,
        REFERENCE_WORK,
        VIDEO
    }

    public static String getContentTypeText(contentType contentType) {
        switch (contentType) {
            case ARTICLE -> {
                return "Article";
            }
            case CHAPTER -> {
                return "Chapter";
            }
            case CONFERENCE_PAPER -> {
                return "Conference Paper";
            }
            case REFERENCE_WORK_ENTRY -> {
                return "Reference Work Entry";
            }
            case BOOK -> {
                return "Book";
            }
            case PROTOCOL -> {
                return "Protocol";
            }
            case CONFERENCE_PROCEEDINGS -> {
                return "Conference Proceedings";
            }
            case BOOK_SERIES -> {
                return "Book Series";
            }
            case JOURNAL -> {
                return "Journal";
            }
            case VIDEO_SEGMENTS -> {
                return "Video Segment";
            }
            case REFERENCE_WORK -> {
                return "Reference Work";
            }
            case VIDEO -> {
                return "Video";
            }
            default -> {
                return "Article";
            }
        }
    }
}
