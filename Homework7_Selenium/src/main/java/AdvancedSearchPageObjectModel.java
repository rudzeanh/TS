import helpers.SpringerPageObjectModel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class AdvancedSearchPageObjectModel extends SpringerPageObjectModel {

    public enum dateSearchModeEnum {
        BETWEEN,
        IN
    }

    @FindBy(id = "all-words")
    private WebElement containsAllWordsTextField;

    @FindBy(id = "least-words")
    private WebElement containsAtLeastOneOfTheWordsTextField;

    @FindBy(id = "date-facet-mode")
    private WebElement dateSearchModeDropdown;

    @FindBy(id = "facet-start-year")
    private WebElement startYearDateField;

    @FindBy(id = "title-is")
    private WebElement titleContainsTextField;

    @FindBy(id = "submit-advanced-search")
    private WebElement searchButton;

    public AdvancedSearchPageObjectModel(WebDriver driver, String url, boolean reloadPage) {
        super(driver, url, reloadPage);
    }

    public AdvancedSearchPageObjectModel(WebDriver driver) {
        super(driver, "https://link.springer.com/advanced-search", true);
    }

    public void writeContainsAllWordsTextField(String words) {
        containsAllWordsTextField.sendKeys(words);
    }

    public void writeContainsAtLeastOneOfTheWords(String words) {
        containsAtLeastOneOfTheWordsTextField.sendKeys(words);
    }

    public void writeTitleContains(String text) {
        titleContainsTextField.sendKeys(text);
    }

    public void selectDateSearchMode(dateSearchModeEnum dateSearchMode) {
        Select selectDestination = new Select(dateSearchModeDropdown);
        switch (dateSearchMode) {
            case IN -> selectDestination.selectByVisibleText("in");
            case BETWEEN -> selectDestination.selectByVisibleText("between");
            default -> selectDestination.selectByVisibleText("between");
        }
    }

    public void writeStartYear(String startYear) {
        startYearDateField.sendKeys(startYear);
    }

    public void clickSearchButton() {
        searchButton.click();
        // set the timeout
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("results-list")));
    }
}
