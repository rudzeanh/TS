import helpers.SearchResultContentType;
import helpers.SpringerPageObjectModel;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class SearchResultPageObjectModel extends SpringerPageObjectModel {

    @FindBy(id = "number-of-search-results-and-search-terms")
    private WebElement resultStatusText;

    @FindBy(className = "facet-constraint-message")
    private WebElement resultWithinText;

    @FindBy(css = ".pagination .page-number")
    private WebElement pageNumberField;

    @FindBy(id = "results-list")
    private WebElement resultArticlesList;

    public SearchResultPageObjectModel(WebDriver driver, String url, boolean reloadPage) {
        super(driver, url, reloadPage);
    }

    public SearchResultPageObjectModel(WebDriver driver) {
        super(driver, "https://link.springer.com/search", true);
    }

    public String getResultStatusString() {
        return resultStatusText.getText();
    }

    public String getResultWithinString() {
        return resultWithinText.getText();
    }

    public void goToThePage(int pageNumber) {
        pageNumberField.clear();
        pageNumberField.sendKeys("" + pageNumber);
        pageNumberField.sendKeys(Keys.ENTER);
        // set the timeout
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("results-list")));
    }

    public Integer getPageNumber() {
        return Integer.parseInt(pageNumberField.getAttribute("value"));
    }

    public void clickOnTheArticle(String articleName) {
        List<WebElement> articles = resultArticlesList.findElements(By.className("title"));

        for (WebElement article : articles) {
            if (article.getText().equals(articleName)) {
                article.click();
                // set the timeout
                WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
                wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("article")));
                return;
            }
        }
    }

    public void clickOnTheArticle(int articleOrdinalNumber) {
        List<WebElement> articles = resultArticlesList.findElements(By.className("title"));

        if (articles.size() >= articleOrdinalNumber) {
            WebElement article = articles.get(articleOrdinalNumber - 1);
            article.click();
            // set the timeout
            WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("article")));
        }
    }

    public void clickContentType(SearchResultContentType.contentType contentType) {
        WebElement contentTypeElement = getContentTypeWebElement(contentType);
        if (contentTypeElement != null) {

            contentTypeElement.click();
        }
    }

    private WebElement getContentTypeWebElement(SearchResultContentType.contentType contentType) {
        List<WebElement> contentTypes = driver.findElement(By.id("content-type-facet"))
                                        .findElements(By.className("facet-link"));

        String textForSearch = SearchResultContentType.getContentTypeText(contentType);

        for (WebElement element : contentTypes) {
            if (element.findElement(By.className("facet-title")).getText().equals(textForSearch))
                return element;
        }
        return null;
    }
}
