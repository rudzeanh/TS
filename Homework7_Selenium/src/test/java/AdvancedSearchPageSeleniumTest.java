import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class AdvancedSearchPageSeleniumTest {

    WebDriver driver;
    AdvancedSearchPageObjectModel advancedSearchPage;

    @BeforeEach
    public void SetUp() {
        BasicConfigurator.configure();
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        advancedSearchPage = new AdvancedSearchPageObjectModel(driver);
    }

    @AfterEach
    public void closeTheWindow() {
        driver.quit();
    }

    @Test
    public void advancedSearchRedirectToResultPage() {
        advancedSearchPage.writeContainsAllWordsTextField("Page Object Model");
        advancedSearchPage.writeContainsAtLeastOneOfTheWords("Sellenium Testing");
        advancedSearchPage.selectDateSearchMode(AdvancedSearchPageObjectModel.dateSearchModeEnum.IN);
        advancedSearchPage.writeStartYear("2022");
        advancedSearchPage.clickSearchButton();
        Assertions.assertEquals("Search Results - Springer", driver.getTitle());

        String url = driver.getCurrentUrl();
        SearchResultPageObjectModel searchResultPage = new SearchResultPageObjectModel(driver, driver.getCurrentUrl(), false);
        Assertions.assertTrue(searchResultPage.getResultStatusString().contains("Result(s) for"));
    }
}
