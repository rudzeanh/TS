import helpers.SearchResultContentType;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class SearchResultPageSeleniumTest {

    WebDriver driver;
    SearchResultPageObjectModel searchResultPage;

    @BeforeEach
    public void SetUp() {
        BasicConfigurator.configure();
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        searchResultPage = new SearchResultPageObjectModel(driver);
    }

    @AfterEach
    public void closeTheWindow() {
        driver.quit();
    }

    @Test
    public void checkContentType() {
        searchResultPage.clickContentType(SearchResultContentType.contentType.ARTICLE);
        Assertions.assertTrue(searchResultPage.getResultWithinString().contains("within Article"));
    }

    @Test
    public void checkGoToThePage() {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        searchResultPage.goToThePage(5);
        Assertions.assertEquals(5, searchResultPage.getPageNumber());
    }

    @Test
    public void checkChangingTheContentTypeThenPageNumber() {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));

        searchResultPage.clickContentType(SearchResultContentType.contentType.ARTICLE);
        searchResultPage.goToThePage(5);


        Assertions.assertEquals(5, searchResultPage.getPageNumber());
        Assertions.assertTrue(searchResultPage.getResultWithinString().contains("within Article"));
    }
}
