import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ArticlePageSeleniumTest {

    WebDriver driver;
    ArticlePageObjectModel articlePage;

    @BeforeEach
    public void SetUp() {
        BasicConfigurator.configure();
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @AfterEach
    public void closeTheWindow() {
        driver.quit();
    }

    @Test
    public void articleNameTest() {
        articlePage = new ArticlePageObjectModel(driver, "https://link.springer.com/article/10.1007/s42832-021-0104-4", true);
        Assertions.assertEquals("Immediate and long-term effects of invasive plant species on soil characteristics", articlePage.getArticleTitle());
    }

    @Test
    public void articlePublishedDateTest() {
        articlePage = new ArticlePageObjectModel(driver, "https://link.springer.com/article/10.1007/s42832-021-0104-4", true);
        Assertions.assertEquals("30 June 2021", articlePage.getArticlePublishedDate());
    }

    @Test
    public void articleDOITest() {
        articlePage = new ArticlePageObjectModel(driver, "https://link.springer.com/article/10.1007/s42832-021-0104-4", true);
        Assertions.assertEquals("https://doi.org/10.1007/s42832-021-0104-4", articlePage.getArticleDOI());
    }
}
