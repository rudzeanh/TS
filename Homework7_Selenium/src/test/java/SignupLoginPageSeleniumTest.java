import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

public class SignupLoginPageSeleniumTest {

    WebDriver driver;
    SignupLoginPageObjectModel signupLoginPage;

    @BeforeEach
    public void SetUp() {
        BasicConfigurator.configure();
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        signupLoginPage = new SignupLoginPageObjectModel(driver);
    }

    @AfterEach
    public void closeTheWindow() {
        driver.quit();
    }

    @ParameterizedTest(name = "Should be logged in successfully with email {0} and password {1}")
    @CsvFileSource(resources = "/login.csv", numLinesToSkip = 0)
    public void successfulLogin(String firstName, String lastName, String email, String password) {
        signupLoginPage.writeLoginEmail(email);
        signupLoginPage.writeLoginPassword(password);
        signupLoginPage.clickLogIn();

        WebElement authorizedPersonFullName = driver.findElement(By.cssSelector("#user > span"));
        Assertions.assertEquals(firstName + " " + lastName, authorizedPersonFullName.getText());
    }
}
