import helpers.SearchResultContentType;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class AdvancedSearchProcessTest {

    WebDriver driver;
    HomepagePageObjectModel homepage;

    @BeforeEach
    public void SetUp() {
        BasicConfigurator.configure();
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        homepage = new HomepagePageObjectModel(driver);
    }

    @AfterEach
    public void closeTheWindow() {
        driver.quit();
    }

    @Test
    @Order(1)
    public void advancedSearchTest_notAuthorizedSearch_saveArticlesParametersToFile() {
        homepage.clickSearchOptionsButton();;
        homepage.clickAdvancedSearchButtonButton();

        // check that we were really redirected to advanced search page
        Assertions.assertEquals("Advanced Search - Springer", driver.getTitle());

        AdvancedSearchPageObjectModel advancedSearchPage = new AdvancedSearchPageObjectModel(driver, driver.getCurrentUrl(), false);
        advancedSearchPage.writeContainsAllWordsTextField("Page Object Model");
        advancedSearchPage.writeContainsAtLeastOneOfTheWords("Sellenium Testing");
        advancedSearchPage.selectDateSearchMode(AdvancedSearchPageObjectModel.dateSearchModeEnum.IN);
        advancedSearchPage.writeStartYear("2022");
        advancedSearchPage.clickSearchButton();

        // check that we were really redirected to search results page
        Assertions.assertEquals("Search Results - Springer", driver.getTitle());

        SearchResultPageObjectModel searchResultPage = new SearchResultPageObjectModel(driver, driver.getCurrentUrl(), false);
        // check applied search query
        Assertions.assertTrue(searchResultPage.getResultStatusString().contains("Page AND Object AND Model AND (Sellenium OR Testing)"));

        searchResultPage.clickContentType(SearchResultContentType.contentType.ARTICLE);

        // check that filter applied
        Assertions.assertTrue(searchResultPage.getResultWithinString().contains("within Article"));

        StringBuilder output = new StringBuilder();

        for (int i = 1; i <= 4; i++) {
            searchResultPage.clickOnTheArticle(i);
            ArticlePageObjectModel articlePage = new ArticlePageObjectModel(driver, driver.getCurrentUrl(), false);
            output.append(articlePage.getArticleTitle()).append(", ");
            output.append(articlePage.getArticlePublishedDate()).append(", ");
            output.append(articlePage.getArticleDOI()).append("\n");
            articlePage.goToThePreviousPage();
            searchResultPage = new SearchResultPageObjectModel(driver, driver.getCurrentUrl(), false);
        }

        try {
            File file = new File("src/test/resources/articles.csv");
            file.createNewFile();

            FileWriter writer = new FileWriter("src/test/resources/articles.csv");
            writer.write(output.toString());
            writer.close();

        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    @Order(2)
    @ParameterizedTest(name = "Articles with parameters: name {0}, published date {1}, DOI {2} should be found")
    @CsvFileSource(resources = "/articles.csv", numLinesToSkip = 0)
    public void advancedSearchTest_searchByName(String articleName, String articlePublishedDate, String articleDOI) {
        homepage.clickSignupLoginButton();

        // check that we were really redirected to the login/registration page
        Assertions.assertEquals("Create Account - Springer", driver.getTitle());

        SignupLoginPageObjectModel signupLoginPage = new SignupLoginPageObjectModel(driver, driver.getCurrentUrl(), false);
        signupLoginPage.writeLoginEmail("selenium-test@yopmail.com");
        signupLoginPage.writeLoginPassword("qwerty12");

        signupLoginPage.clickLogIn();

        // check that we logged in successfully
        WebElement authorizedPersonFullName = driver.findElement(By.cssSelector("#user > span"));
        Assertions.assertEquals("Selenium Test", authorizedPersonFullName.getText());

        homepage.clickSearchOptionsButton();
        homepage.clickAdvancedSearchButtonButton();

        // check that we were really redirected to advanced search page
        Assertions.assertEquals("Advanced Search - Springer", driver.getTitle());

        AdvancedSearchPageObjectModel advancedSearchPage = new AdvancedSearchPageObjectModel(driver, driver.getCurrentUrl(), false);
        advancedSearchPage.writeTitleContains(articleName);

        advancedSearchPage.clickSearchButton();

        // check that we were really redirected to search results page
        Assertions.assertEquals("Search Results - Springer", driver.getTitle());

        SearchResultPageObjectModel searchResultPage = new SearchResultPageObjectModel(driver, driver.getCurrentUrl(), false);

        searchResultPage.clickOnTheArticle(articleName);

        ArticlePageObjectModel articlePage = new ArticlePageObjectModel(driver, driver.getCurrentUrl(), false);

        Assertions.assertEquals(articlePublishedDate, articlePage.getArticlePublishedDate());
        Assertions.assertEquals(articleDOI, articlePage.getArticleDOI());
    }
}
