import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.log4j.BasicConfigurator;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class HomepageSeleniumTest {

    WebDriver driver;
    HomepagePageObjectModel homepage;

    @BeforeEach
    public void SetUp() {
        BasicConfigurator.configure();
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        homepage = new HomepagePageObjectModel(driver);
    }

    @AfterEach
    public void closeTheWindow() {
        driver.quit();
    }

    @Test
    public void fromHomepage_RedirectToSignupLoginPage() {
        homepage.clickSignupLoginButton();
        Assertions.assertEquals("Create Account - Springer", driver.getTitle());
    }

    @Test
    public void fromHomepage_RedirectToAdvancedSearchPage() {
        homepage.clickSearchOptionsButton();
        homepage.clickAdvancedSearchButtonButton();
        Assertions.assertEquals("Advanced Search - Springer", driver.getTitle());
    }
}
