package Lesson3;

public class Number {

    private int number = 0;

    public int ReturnFive() {
        return 5;
    }

    public int GetPrivateNumber() {
        return number;
    }

    public void IncrementPrivateNumber() {
        number++;
    }

    public void ThrowException() throws Exception {
        throw new Exception("Expected error");
    }
}
