package Lesson3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class NumberTest {

    private Number number;

    // MethodName_TestingState_ExpectedOutput

    // Setup
    @BeforeEach
    public void RunBeforeEachTest() {
        number = new Number();
    }

    @Test
    public void ReturnFive_ReturningSpecificNumber_Five() {
        // arrange
        int expectedValue = 5;

        // act
        int result = number.ReturnFive();

        // assert
        Assertions.assertEquals(expectedValue, result);
    }

    @Test
    public void GetPrivateNumber_ReturningPrivateZero_Zero() {
        // arrange
        int expectedValue = 0;

        // act
        int result = number.GetPrivateNumber();

        // assert
        Assertions.assertEquals(expectedValue, result);
    }

    @Test
    public void IncrementPrivateNumber_IncrementedNumberIsGreaterByOne_One() {
        // arrange
        int expectedValue = number.GetPrivateNumber() + 1;

        // act
        number.IncrementPrivateNumber();
        int result = number.GetPrivateNumber();

        // assert
        Assertions.assertEquals(expectedValue, result);
    }

    @Test
    public void ThrowException_ThrowingException_Exception() {
        // act and assert
        Assertions.assertThrows(Exception.class, () -> number.ThrowException());
    }
}
